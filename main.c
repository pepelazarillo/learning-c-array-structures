#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

/* Explicitly declaring the array samples size */
#define LEN 10

/* Structure to collect temperature samples */
typedef struct Sample
{
    double temperature;
    time_t timestamp; // we need to include time.h to use time_t data type
}Sample;

/* Generate temperature values for the sample */
int rand_range_int(int min, int max)
{
    /* min and max represent the upper and lower limit for the random number, the we scale it up with the lower bound*/
    return rand() % (max - min +1) + min;
}

/* Convert integer to random floating point */
double rand_range(double min, double max, int decimal_digits)
{
    double scale_factor = pow(10, decimal_digits);  // for 1 digit we scale by 10, 2 digits by 100 and so on
    int scaled_min = round(min * scale_factor);
    int scaled_max = round(max * scale_factor);
    return rand_range_int(scaled_min, scaled_max) / scale_factor;
}

double get_temperature(double min, double max, int decimal_digits)
{
    return rand_range(min, max, decimal_digits);
}

int main() {

    printf("\n=== Array of Structures === \n");
    Sample samples[LEN];

    double min_temp;
    double max_temp;
    int decimal_digits;

    /* Acquire entries from user */
    printf("Enter min temperature range [i.e: 19.3];\n");
    scanf("%lf", &min_temp);

    printf("Enter max temperature range [i.e: 19.3];\n");
    scanf("%lf", &max_temp);

    printf("Enter number of digits [i.e: 1,2,3..10];\n");
    scanf("%d", &decimal_digits);


    /* Generate random temperatures to fill the temperature logger */
    for (int i = 0; i < LEN; ++i) {
        printf("Sample %d...\n", i);

        /* Acquire temperature and timestamp in each i lecture */
        samples[i].timestamp = time(NULL);  // time() returns the universal time in seconds
        samples[i].temperature = get_temperature(min_temp, max_temp, decimal_digits);

        /* With this instruction, we are putting our processes to sleep for one second , we include unistd.h*/
        sleep(1);
    }

    /* print: timestamp, time offset from first sample, temperature */
    printf("\ntimestamp, time offset, temp\n");

    for (int i = 0; i < LEN; ++i) {
        printf("%ld, %ld, %5.1f\n",
               samples[i].timestamp,
               samples[i].timestamp - samples[0].timestamp,
               samples[i].temperature);
    }

    return EXIT_SUCCESS;
}
