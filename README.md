# How to declare and use a structures array:
## Temperature logger
* This project build a temperature logger simulating random variations of temperatures within a specific range
* We use structures arrays to track temperatures and timestamps

### Note:
In order to include the math.h in CLion you have to include this line in the CMakeLists.txt file:
* target_link_libraries(arrays m)
* arrays in this case is the name of the project